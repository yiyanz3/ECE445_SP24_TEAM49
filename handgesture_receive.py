import cv2
import socket
from PIL import Image
import io
import numpy as np
import mediapipe as mp
import time
import math
import requests
import threading
import time


# 全局变量用于记录上次发送时间
last_sent_time = 0
fist_counter = 0
palm_counter = 0
last_message = 'None'
trackingmode = 1
colors = ['','D0104C', '90B44B', '00AA90', '4E4F97', 'E03C8A']
def send_message(esp32_ip, message):
    global fist_counter
    global palm_counter
    global trackingmode
    if message == 'None':
        return
    """发送消息到指定的ESP32设备，处理可能的异常。"""
    try:

        if message == 'fist' and trackingmode == 1:
            if fist_counter == 0:
                fist_counter += 1
            elif  fist_counter < 3:
                fist_counter += 1

                message = f'RGB:{colors[fist_counter]}'
            else:
                message = 'trackingmode:0'
                trackingmode = 0
                fist_counter = 0
        else:
            fist_counter = 0
        if message == 'fist' and trackingmode == 0:
            if palm_counter == 0:
                palm_counter += 1
            elif  palm_counter < 3:
                palm_counter += 1

                message = f'RGB:{colors[4-palm_counter ]}'
            else:
                message = 'trackingmode:1'
                trackingmode = 1
                palm_counter = 0
        else:
            palm_counter = 0
        if message == 'one':
            message = 'trackingmode:1'
        if message == 'two':
            message = 'trackingmode:0'
        if message == 'three':
            message = 'crazymode:1'
        print(f'Sent: {message}')  # 显示发送的消息


            
                
        url = f'http://{esp32_ip}/?message={message}'
        response = requests.get(url, timeout=5)


    except requests.exceptions.RequestException as e:
        print(e)  # 打印异常信息

def update_message(new_message):
    global last_sent_time
    # global last_message
    current_time = time.time()

    if current_time - last_sent_time >= 2:  # 检查是否已经过了至少2秒
        send_message(esp32_ip, new_message)
        last_sent_time = current_time  # 更新最后发送时间

# 启动请求线程
#start_request_thread('192.168.157.165')

def calculate_angle_between_slopes(slope1, slope2):
    # 将斜率转换为角度
    angle1 = math.degrees(math.atan(slope1))
    angle2 = math.degrees(math.atan(slope2))

    # 计算两个角度之间的最小夹角
    angle_difference = abs(angle1 - angle2)
    
    # 因为两条直线的夹角最大为180度
    if angle_difference > 180:
        angle_difference = 360 - angle_difference

    return angle_difference
def is_finger_active(tip, prev_joint, prev_prev_joint, wrist):
    # 计算斜率
    def slope(p1, p2):
        dx = p2.x - p1.x
        dy = p2.y - p1.y
        if dx == 0:  # 避免除以零
            return float('inf')  # 无限大的斜率表示垂直
        return dy / dx
    
    # 计算距离的平方，避免开根号以提高性能
    def distance_squared(p1, p2):
        dx = p2.x - p1.x
        dy = p2.y - p1.y
        dz = p2.z - p1.z
        return dx**2 + dy**2 + dz**2
    
    # 计算各斜率
    slope_tip_prev = slope(tip, prev_joint)
    slope_wrist_prevprev = slope(wrist, prev_prev_joint)
    slope_prev_prevprev = slope(prev_joint, prev_prev_joint)

    # 计算斜率变化
    slope_diff_1 = abs(slope_tip_prev - slope_prev_prevprev)
    slope_diff_2 = abs(slope_prev_prevprev - slope_wrist_prevprev)
    slope_change_threshold = 3

    # 计算距离
    dist_tip_to_wrist = distance_squared(tip, wrist)
    dist_prev_joint_to_wrist = distance_squared(prev_joint, wrist)

    # 判断手指是否弯曲
    is_bent_by_slope = slope_diff_1 > slope_change_threshold or slope_diff_2 > slope_change_threshold
    is_bent_by_distance = dist_tip_to_wrist < dist_prev_joint_to_wrist

    # 如果手指因为斜率变化大或指尖比上一个关节更接近手腕，则认为手指弯曲（deactive）
    return not (is_bent_by_slope or is_bent_by_distance)
def is_finger_active_thumb(tip, prev_joint,prev_prev_joint ,wrist):
    def slope(p1, p2):
        dx = p2.x - p1.x
        dy = p2.y - p1.y
        if dx == 0:  # 避免除以零
            return float('inf')  # 无限大的斜率表示垂直
        return dy / dx
    def distance_squared(p1, p2):
        dx = p2.x - p1.x
        dy = p2.y - p1.y
        dz = p2.z - p1.z
        return dx**2 + dy**2 + dz**2
    dist_tip_to_wrist = distance_squared(tip, wrist)
    dist_prev_joint_to_prev_prev_joint = distance_squared(prev_joint, prev_prev_joint)
    dist_prev_prev_joint_to_wrist = distance_squared(prev_prev_joint, wrist)
    dist_tip_to_prev_joint = distance_squared(tip, prev_joint)
    
    
    slope_tip_prev = slope(wrist, prev_joint)
    slope_wrist_prevprev = slope(wrist, prev_prev_joint)
    angle = calculate_angle_between_slopes(slope_tip_prev, slope_wrist_prevprev)
    if dist_tip_to_wrist <(dist_prev_joint_to_prev_prev_joint+dist_prev_prev_joint_to_wrist+dist_tip_to_prev_joint)*1.5 or angle >30:
        return False
    else:
        return True

def thumb_direction(thumb, base):
    global intensity
    dx = thumb["x"] - base["x"]
    dy = thumb["y"] - base["y"]
    if dx == 0:
        slope = float('inf')
    else:
        slope = dy / dx
    if slope > 1 or slope < -1:
        if dy > 0:
            if (intensity>=5):
                intensity -=5
            if intensity < 5:
                intensity = 0
            message = f"intensity:{intensity}"

            update_message(message)

            print(message)
        else:
            if (intensity<=95):
                intensity += 5
            if intensity > 95:
                intensity = 100
            message = f"intensity:{intensity}"


            update_message(message)

            print(message)
    elif slope >= -1 and slope <= 1:
        if dx > 0:
            update_message('turnright')
            print('right')
        else:
            update_message('turnleft')
            print('left')
    # message = 'HelloESP32'
    #update_message(message)
    # 发送GET请求
    #response = requests.get(url)



# 初始化 MediaPipe 手势识别
esp32_ip = '192.168.104.165'
message='None'
intensity = 100

mp_hands = mp.solutions.hands
hands = mp_hands.Hands(
    static_image_mode=False,
    max_num_hands=2,
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5)
mp_draw = mp.solutions.drawing_utils

# 创建UDP socket接收图像
server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(("0.0.0.0", 9090))
server_socket.settimeout(1.0)

# 图片尺寸
video_width = 320
video_height = 240
center_point = (video_width // 2, video_height // 2)

p_time = 0
c_time = 0
thumb_tip_list=[0,0,0]
thumb_base_list=[0,0,0]
distanceDict = {}
finger_tip_coordinates = {}

fingers = {
    "Thumb": (4, 3,1),
    "Index": (8, 7,5),
    "Middle": (12, 11,9),
    "Ring": (16, 15,13),
    "Pinky": (20, 19,17)
}
finger_info = {}  # 用于存储每个手指的信息

while True:
    try:
        #time.sleep(1)
        # 接收图像数据
        message1, address = server_socket.recvfrom(1024 * 64)  # 假设最大图片大小为64KB
        time1 = time.time()
        print(time1)
        if message1:
            # 将接收到的数据转换为OpenCV图像
            image = Image.open(io.BytesIO(message1))
            frame = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
            frame = cv2.resize(frame, (video_width, video_height))
            frame = cv2.flip(frame, 0)

            # 应用手势识别逻辑
# 应用手势识别逻辑
            imgRGB = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = hands.process(imgRGB)

            # 绘制手势标识和判断手指激活状态
            # 继续你的主循环逻辑...
            if results.multi_hand_landmarks:
                for hand_landmarks in results.multi_hand_landmarks:
                    mp_draw.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                    
                    wrist = hand_landmarks.landmark[0]
                    
                    for finger_name, (tip_id, prev_joint_id,prev_prev_joint_id) in fingers.items():
                        tip = hand_landmarks.landmark[tip_id]
                        prev_joint = hand_landmarks.landmark[prev_joint_id]
                        prev_prev_joint = hand_landmarks.landmark[prev_prev_joint_id]
                        if finger_name == "Thumb":
                            is_active= is_finger_active_thumb(tip, prev_joint,prev_prev_joint ,wrist)

                        else:
                            is_active = is_finger_active(tip, prev_joint,prev_prev_joint ,wrist)
                            # 存储手指的x, y, z坐标和激活状态
                        finger_info[finger_name] = {
                            "x": tip.x,
                            "y": tip.y,
                            "z": tip.z,
                            "active": is_active
                        }
                    
                    
                    # 在此处可以对finger_info字典进行操作，例如打印或进一步处理
                    # print(finger_info)
                    if (finger_info["Thumb"]["active"] and
                        not finger_info["Index"]["active"] and
                        not finger_info["Middle"]["active"] and
                        not finger_info["Ring"]["active"] and
                        not finger_info["Pinky"]["active"]):
                        thumb = {"x": finger_info["Thumb"]["x"], "y": finger_info["Thumb"]["y"], "z": finger_info["Thumb"]["z"]}
                        base = {"x": wrist.x, "y": wrist.y, "z": wrist.z}
                        thumb_direction(thumb, base)  # 调用thumb_direction函数
                    if(finger_info["Thumb"]["active"] and finger_info["Index"]["active"] and finger_info["Middle"]["active"] and finger_info["Ring"]["active"] and finger_info["Pinky"]["active"]):
                        print("Palm")
                        message = 'palm'

                    if (not finger_info["Thumb"]["active"] and
                        not finger_info["Index"]["active"] and
                        not finger_info["Middle"]["active"] and
                        not finger_info["Ring"]["active"] and
                        not finger_info["Pinky"]["active"]):
                        print("Fist")
                        message = 'fist'
                    if (finger_info["Index"]["active"] and
                        finger_info["Middle"]["active"] and
                        not finger_info["Ring"]["active"] and
                        not finger_info["Pinky"]["active"]):

                        print("two")
                        message = 'two'
                    if (finger_info["Index"]["active"] and
                        not finger_info["Middle"]["active"] and
                        not finger_info["Ring"]["active"] and
                        not finger_info["Pinky"]["active"]):

                        print("one")
                        message = 'one'

                    update_message(message)
                                
            # 显示图像


            #cv2.imshow("Received Frame", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    except socket.timeout:
        print("No image received in the last interval.")
        continue
        #break
    except Exception as e:
        print(f"Unexpected error: {e}")
        break

# 清理资源
hands.close()
server_socket.close()
cv2.destroyAllWindows()
