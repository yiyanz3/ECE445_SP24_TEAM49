import socket
import network
import camera
import time

def connect_to_wifi(ssid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('Connecting to network...')
        wlan.connect(ssid, password)
        
        for _ in range(10):
            if wlan.isconnected():
                print('Network configuration:', wlan.ifconfig())
                return True
            time.sleep(1)
    else:
        print('Already connected. Network configuration:', wlan.ifconfig())
        return True
    print('Failed to connect to WiFi.')
    return False

def init_camera():
    try:
        camera.init()
        camera.framesize(10)    # frame size 800X600 (1.33 aspect ratio)
        camera.contrast(2)      # increase contrast
        camera.speffect(2)      # jpeg grayscale
        print("Camera initialized")
    except Exception as e:
        print("Failed to initialize camera:", e)
        camera.deinit()
        return False
    return True

def main():
    if not connect_to_wifi('转生成为一般居民', 'zyy010403'):
        if not connect_to_wifi('Hypermuteki', 'zyy010425'):
        
            print("WiFi connection failed, exiting...")
            return
    
    if not init_camera():
        print("Camera initialization failed, exiting...")
        return
    
    # Socket UDP creation
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    try:
        while True:
            buf = camera.capture()  # Capture image data
            try:
                s.sendto(buf, ("192.168.50.235", 9090))  # Send image data to the server
                print(f"Sending {len(buf)} bytes")
            except Exception as e:
                print("Failed to send data:", e)
            
            time.sleep(0.1)
    except KeyboardInterrupt:
        print("Program interrupted")
    finally:
        camera.deinit()
        s.close()
        print("Cleaned up resources.")

if __name__ == "__main__":
    main()
